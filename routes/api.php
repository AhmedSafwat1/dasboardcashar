<?php

header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");



//** Start basic info**//
Route::group( ['namespace' => 'API'], function() {
    //main info retertive all cities
    Route::post('/get_cities'           ,'BasicInfoController@GetCities');
    //banks accoutnts
    Route::post('/get_accoutnts'         ,'BasicInfoController@GetAccounts');
    //banks transfers
    Route::post('/bank_transfer'         ,'BasicInfoController@moneyTransfer');
    ////get products from all shop
    Route::post('/all_products'          ,'BasicInfoController@shopsProduct');
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            Route::post('/tranfer_money'          ,'BasicInfoController@moneyTransfer');


});
// user controller
Route::group( ['namespace' => 'API'], function() {
    // sign in route
    Route::post('/signup'               ,'UserController@signUp');
    //check code
    Route::post('/check_code'           ,'UserController@Check_Code');
    //resend the code
    Route::post('/resend_code'          ,'UserController@ResendCode');
    //forget password
    Route::post('/forgot-password'      ,'UserController@forgotPassword');
    //change passorod
    Route::post('/change_password'      ,'UserController@changePassword');
    // REST password
    Route::post('/reset-password'       ,'UserController@resetPassword');
    //login
    Route::post('/signin'               ,'UserController@signIn');
    // get user info
    Route::post('/get_user'             ,'UserController@GetUser');
    // update user profile
    Route::post('/update_profile'       ,'UserController@updateProfile');
    // update user profile
    Route::post('/change_password'      ,'UserController@changePassword');
    // home after login
    Route::post('/home'                 ,"UserController@homeUser");
    // favourit product
    Route::post('/favourit'             ,"UserController@userFavourit");
    // add and reomvoe favourit
    Route::post('/favourite_action'     ,'UserController@postFavourite');
    // add Rate
    Route::post('/add_rate'             ,'UserController@postRate');
});
// end user controller
// shop Controller
Route::group( ['namespace' => 'API'], function() {
    //all shops avaialbale
    Route::post('/get_shops'               ,'ShopController@GetShops');
    //search by name
    Route::any('/search'                   ,'ShopController@search');
    //get shop info
    Route::post('/get_shop'                ,'ShopController@getShop');
    //get products from shop based in category
    Route::post('/get_products'             ,'ShopController@shopProduct');
    //get product from shop based in id
    Route::post('/get_product'             ,'ShopController@ProductInnfo');
});
//end shops controller
//start order Controler
Route::group( ['namespace' => 'API'], function() {
    // get current shop card / current order
    Route::post('/current_order'           ,'OrderController@current_order');
    //add to card;
    Route::post('/add_card'               ,'OrderController@add_card');
    //add to card;
    Route::post('/save_order'             ,'OrderController@save_order');
    //has orders;
    Route::post('/hasOrders'             ,'OrderController@hasOrders');

});
//** Start SettingController**//
Route::group( ['namespace' => 'API'], function() {
    Route::any('phone-keys'         ,'SettingController@phoneKeys');
    Route::any('sign-up'            ,'AuthController@signUp');
    Route::any('sign-in'            ,'AuthController@signIn');
    Route::any('forget-password'    ,'AuthController@forgetPassword');
    Route::any('update-password'    ,'AuthController@updatePassword');
});
//** End SettingController**//

Route::group(['middleware' => ['mobile'] , 'namespace' => 'API'], function() {

         Route::any('edit-profile'            ,'AuthController@editProfile');
         Route::any('edit-password'           ,'AuthController@editPassword');

});