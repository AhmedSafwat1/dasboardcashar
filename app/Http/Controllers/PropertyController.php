<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use App\Property;
use Illuminate\Http\Request;

class PropertyController extends Controller
{
    //
    //
    // display properties
    public function properties()
    {
        
        $properties   = Property::all();
        return view('dashboard.properties.properties',compact('properties'));
    }
    // add Property
    public function add(Request $request)
    {
        // dd($request->all());
        $customError  = array(
            'name_ar.required'       => "اسم الخصيه باللغه العربيه مطلوب", 
            'name_en.required'       => "اسم الخصيه باللغه الانجليزىه  مطلوب",
            'name_ar.min'            => " اسم الخصيه باللغه العربيه لايقل عن 3 حروف", 
            'name_en.min'            => " اسم الخصيه باللغه الانجليزيه لايقل عن 3 حروف", 
            'name_ar.max'            => " اسم الخصيه باللغه العربيه لايزيد  عن 190 حروف", 
            'name_en.max'            => " اسم الخصيه باللغه العربيه لايزيد  عن 190 حروف", 
            
        );
        $this->validate($request, [
            'name_ar' => 'required|min:3|max:190',
            'name_en' => 'required|min:3|max:190',
        
        ], $customError);
        
        Property::create($request->all());
        Report(Auth::user()->id,'بأضافة خصيه جديد');
        Session::flash('success','تم اضافة خصيه');
		return back();
    }
    // delete Property
    public function delete(Request $request)
    {
        // dd($request->all());
        $Property = Property::findOrFail($request->id);
        $Property->delete() ;
        Report(Auth::user()->id,'بحذف الخصيه '.$Property->name_ar);
        Session::flash('success','تم الحذف');
        return back();
    }
    //edit
    public function edit(Request $request)
    {
        $Property = Property::findOrFail($request->id);
        // dd($request->all());
        // $category = Category::findOrFail($request->category_id);
        $customError  = array(
            'edit_name_ar.required'       => "اسم الخصيه باللغه العربيه مطلوب", 
            'edit_name_en.required'       => "اسم الخصيه باللغه الانجليزىه  مطلوب",
            'edit_name_ar.min'            => " اسم الخصيه باللغه العربيه لايقل عن 3 حروف", 
            'edit_name_en.min'            => " اسم الخصيه باللغه الانجليزيه لايقل عن 3 حروف",
            'edit_name_ar.max'            => " اسم الخصيه باللغه العربيه لايزيد  عن 190 حروف", 
            'edit_name_en.max'            => " اسم الخصيه باللغه العربيه لايزيد  عن 190 حروف", 
        );
        $this->validate($request, [
            'edit_name_ar' => 'required|min:3|max:190',
            'edit_name_en' => 'required|min:3|max:190',
        ], $customError);
      
      
        $Property->name_ar = $request->edit_name_ar;
        $Property->name_en = $request->edit_name_en;
        $Property->save();
        
        Report(Auth::user()->id,'قام بتعديل الخصيه '.$Property->name);
        Session::flash('success','تم تعديل');
        return back();
    }
}
