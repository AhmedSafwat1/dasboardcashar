<?php

namespace App\Http\Controllers\API;

use App\Addtion;
use App\Card;
use App\Order;
use App\Product;
use App\Shop;
use App\User;
use App\Size;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Validator;
use Auth;
use URL;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
class OrderController extends Controller
{

    // get current order / shop card
    public function current_order(Request $request)
    {

        $validator=Validator::make($request->all(),[
            'lang'    => 'required',
            'user_id' => 'required|exists:users,id',
//            'status'  => 'required|integer|min:0|max:7',
            "order_id" => 'required|exists:orders,id'

        ],[
            "user_id.exists" =>$request['lang'] == 'ar' ? "المستخدم غير موجود" : "user not found"
            ,"order_id.exists" =>$request['lang'] == 'ar' ? "الطلب غير موجود" : "order not found"
        ]);

        if ($validator->passes()) {
            $lang = $request["lang"];
            $order  =  Order::where("user_id" ,"=", $request["user_id"])
                            ->where("id","=", $request["order_id"])->first();
            if(!$order)
            {
                return response()->json(['key'=>'success','value'=>'1','data'=>[] ]);
            }
            else
            {
                $data             = [];
                $data["order_id"] = $order["id"];
                if(count($order->Cards) > 0) {
                    $cards = $order->Cards->map(function ($card) use ($lang) {
                        $res["id"] = $card["id"];
                        $res["product_name"] = $lang == "ar" ? $card->Size->Product->name_ar : $card->Size->Product->name_en;
                        $res["product_size"] = $card->Size->size;
                        $res["product_qunatity"] = $card->Size->quantity;
                        $res["price"] = $card->Size->price;
                        $res["Haveaddation"] = !is_null($card->addtions);
                        $price = 0;
                        $res["addtions"] = is_null(json_decode($card->addtions, true)) ? [] : json_decode($card->addtions, true);

                        if ($res["Haveaddation"] && $card->addtions) {
                            foreach (json_decode($card->addtions, true) as $add) {
                                $price += $add["price"] * $add["quntity"] ;
                            }
                        }
                        $res["addtion_price"] = $price;
                        return $res;
                    });
                    $data["cards"] = $cards->toArray();
                    $shop = $order->Shop;
                    $sdata["id"] = $shop->id;
                    $sdata["image"] = URL::to('dashboard/uploads/shops') . '/' . $shop['image'];
                    $sdata["name"] = $lang == "ar" ? $shop["name_ar"] : $shop["name_en"];
                    $sdata["lat"] = $shop->lat;
                    $sdata["lng"] = $shop->lat;
                    $data["shop"] = $sdata;
                    $data["total_price"] = array_sum($order->Cards->pluck('total')->toArray()) + $order['delivery_coast'];

                }
                else
                {
                    $data["cards"]       = [];
                    $data["shop"]        = [];
                    $data["total_price"] = 0;
                }

                $data["order_status"]   = $order->status;
                $data["order_prepared"] = $order->prepared;
                $data["order_payment"]  = $order->Payment ;
                $data["delivery_coast"] = $order->delivery_coast;
                $data["order_created_at_full"] = $order->created_at->toDateTimeString();
                //               dd( \Carbon::setLocale('ar')->parse($order->created_at)->format('H:i:a'));
                $data["order_created_at"] = $order->created_at->format('d/m/Y');
                $data["order_created_at_time"] = $order->created_at->format('H:i:a');
                return response()->json(['key'=>'success','value'=>'1','data'=>$data ]);
            }

        }else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    // add to shop card
    public function add_card(Request $request)
    {
        $niceError=[
            "user_id.exists"    =>$request['lang'] == 'ar' ? "المستخدم غير موجود" : "user not found",
            "shop_id.exists"    =>$request['lang'] == 'ar' ? "المطعم غير موجود" : "shop not found",
            "quantity.required" =>$request['lang'] == 'ar' ? "الكميه  مطلوبه" : "quantity not found",
            "quantity.numeric"  =>$request['lang'] == 'ar' ? "الكميه  يجب ان تكون رقم" : "quantity not number",
            "quantity.min"      =>$request['lang'] == 'ar' ? "الكميه  يجب ان تكون 1 على الاقل" : "quantity must at least 1",
            "size_id.exists"    =>$request['lang'] == 'ar' ? "المنتج غير موجود" : "product not found"
        ];
        $validator=Validator::make($request->all(),[
            'lang'       => 'required',
            'user_id'    => 'required|exists:users,id',
            'shop_id'    => 'required|exists:shops,id',
            'size_id'    => 'required|exists:shops,id',
            'quantity'   => 'required|numeric|min:1'

        ],$niceError);
        if ($validator->passes()) {
            $order = Order::where("user_id", $request['user_id'])
                ->where("status", 0)->first();
            $product = Size::find($request["size_id"]);
            if($order->shop_id != $request["shop_id"])
            {
                $order->Cards()->delete();
                $order->shop_id = $request["shop_id"];
                $order->update();
            }
            if ($product->quantity < $request["quantity"]) {
                $msg = $request["lang"] == "ar" ? "الكميه المطلوبه غير متاحه حاليا" : " the quantity from product not availabel now";
                return response()->json(['key' => 'fail', 'value' => '0', 'msg' => $msg]);
            }
            $card = new Card;
            $card->user_id = $request['user_id'];
            $card->size_id = $request['size_id'];
            $card->quantity = $request['quantity'];
            $card->price = $product->price;
            $card->total = $product->price * $request['quantity'];
            if ($order) {
                $card->order_id = $order->id;

            } else {
                $shop = Shop::find($request['shop_id']);
                $order = new Order;
                $order->user_id = $request['user_id'];
                $order->shop_id = $request['shop_id'];
                $order->delivery_coast = $shop->delivery_coast;
                $order->lat = $shop->lat;
                $order->lng = $shop->lng;
                $order->save();
                $card->order_id = $order->id;
            }
            if($request["addtions"])
            {
                $addtions = json_decode($request["addtions"]);
                foreach ($addtions as $addtion) {
                    $ad = Addtion::find($addtion["id"]);
                    if ($ad && $ad->quantity < $addtion->quantity) {
                        $msg = $request["lang"] == "ar" ? "الكميه المطلوبه من الاضافه " . $ad["name_ar"] . "غير متاحه حاليا" : " the quantity from " . $ad["name_en"] . "not availabel now";
                        return response()->json(['key' => 'fail', 'value' => '0', 'msg' => $msg]);
                    }
                    $card->total = $addtion->total;
                }
                $card->addtions = $request["addtions"];
            }
            $card->save();
            $msg = $request["lang"] == "ar" ? "تم اضافة الطلب الى السله" : " successfull add order the shop card";
            return response()->json(['key' => 'success', 'value' => '1', 'msg' => $msg]);
        }
        else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    //save card
    private function save_card($order, $cardDate ,$lng)
    {
        $cardDate = (array)json_decode($cardDate);
        $product = Size::find($cardDate["size_id"]);
        if ($product->quantity < $cardDate["quantity"]) {
            $msg = $lng == "ar" ? "الكميه المطلوبه من المنتج ".$product->Product->name_ar." غير متاحه حاليا" : " the quantity from product ".$product->Product->name_en." not availabel now";
            return ['key' => 'fail', 'value' => '0', 'msg' => $msg];
        }
        $product->quantity -= $cardDate["quantity"];
        $product->save();
        $card               = new Card;
        $card->user_id      = $order['user_id'];
        $card->size_id      = $cardDate['size_id'];
        $card->quantity     = $cardDate['quantity'];
        $card->price        = $product->price;
        $card->total        = $card->price * $cardDate['quantity'];
        $card->order_id     = $order->id;
        if($cardDate["addtions"])
        {

            $addtions = $cardDate["addtions"];
            foreach ($addtions as $addtion) {

                $ad = Addtion::find($addtion->id);
                if ($ad && $ad->quantity < $addtion->quantity) {
                    $msg = $lng == "ar" ? "الكميه المطلوبه من الاضافه " . $ad["name_ar"] . " غير متاحه حاليا" : " the quantity from " . $ad["name_en"] . "not availabel now";
                    return ['key' => '0', 'msg' => $msg];
                }
                $ad->quantity =  $ad->quantity -$addtion->quantity;
                $ad->save();
                $card->total = $addtion->price * $addtion->quantity  ;
            }

            $card->addtions = json_encode($cardDate["addtions"]);

        }
        $card->save();
        return ['key' => '1', 'msg' => $card->toArray()];
    }
    //check quantity
    private function check_card( $cardDate ,$lng)
    {
        $cardDate = (array)json_decode($cardDate);
        $product = Size::find($cardDate["size_id"]);
        if ($product->quantity < $cardDate["quantity"]) {
            $msg = $lng == "ar" ? "الكميه المطلوبه من المنتج ".$product->Product->name_ar." غير متاحه حاليا" : " the quantity from product ".$product->Product->name_en." not availabel now";
            return ['key' => 'fail', 'value' => '0', 'msg' => $msg];
        }
        if($cardDate["addtions"])
        {

            $addtions = $cardDate["addtions"];
            foreach ($addtions as $addtion) {

                $ad = Addtion::find($addtion->id);
                if ($ad && $ad->quantity < $addtion->quantity) {
                    $msg = $lng == "ar" ? "الكميه المطلوبه من الاضافه " . $ad["name_ar"] . " غير متاحه حاليا" : " the quantity from " . $ad["name_en"] . "not availabel now";
                    return ['key' => '0', 'msg' => $msg];
                }
            }



        }
        return ['key' => '1', 'msg' => "validation"];
    }
    // save order
    public function save_order(Request $request)
    {
        $niceError = [
            "user_id.exists"   => $request['lang'] == 'ar' ? "المستخدم غير موجود" : "user not found",
            "shop_id.exists"   => $request['lang'] == 'ar' ? "المطعم غير موجود" : "shop not found",
            "payment.required" => $request['lang'] == 'ar' ? "طريقة الدفع مطلوبه " : "payment type reqiured ",
        ];
        $validator = Validator::make($request->all(), [
            'lang'    => 'required',
            'user_id' => 'required|exists:users,id',
            'shop_id' => 'required|exists:shops,id',
            'cards'   => "required|array|min:1",
            "payment" => "required|integer|in:1,0"

        ], $niceError);
        if ($validator->passes()) {
            foreach ($request["cards"] as $card) {
               $val =  $this->check_card($card, $request["lang"]);
                if ($val["key"] == 0) {
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $val["msg"]]);
                }
            }
            DB::beginTransaction();
                $order                 = new Order;
                $shop                  = Shop::find($request['shop_id']);
                $order->user_id        = $request['user_id'];
                $order->shop_id        = $request['shop_id'];
                $order->delivery_coast = $shop->delivery_coast;
                $order->Payment        = $request['payment'];
                $order->lat            = $shop->lat;
                $order->lng            = $shop->lng;
                $order->status         = 1;
                $order->save();
                foreach ($request["cards"] as $card) {
                    $val = $this->save_card($order, $card, $request["lang"]);
                    if ($val["key"] == 0) {
                        DB::rollback();
                        return response()->json(['key' => 'fail','value' => '0', 'msg' => $val["msg"]]);
                    }
                }
                $order["total price"] = array_sum($order->Cards->pluck('total')->toArray()) + $order['delivery_coast'];
                $order["prepared"]    = array_sum($order->Cards->Size->product->pluck('prepared')->toArray());
                $order->save();
                DB::commit();
                $msg = $request["lang"] == "ar" ? "تم عمل الطلب فى انتظار الموافقه من المطعم":"make the order done wait accept from shop";
                return response()->json(['key' => 'success', 'value' => '1', 'msg' => $msg]);

        }
        else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    // get order based the status
    public function hasOrders(Request $request)
    {

        $validator=Validator::make($request->all(),[
            'lang'    => 'required',
            'user_id' => 'required|exists:users,id',
            'shop_id' => 'required|exists:shops,id',
            'status'  => 'required|integer|min:0|max:7',

        ],[
            "user_id.exists" =>$request['lang'] == 'ar' ? "المستخدم غير موجود" : "user not found"
            ,"shop_id.exists" =>$request['lang'] == 'ar' ? "المطعم  غير موجود" : "shops not found"
        ]);

        if ($validator->passes()) {
            $lang = $request["lang"];
            $orders  =  Order::where("user_id" ,"=", $request["user_id"])
                ->where("status" ,"=", $request["status"])
                ->where("shop_id","=", $request["shop_id"])->get();
            $lang = $request["lang"];
            $data = $orders->map(function ($order) use ($lang){
                $res  = [];
                if(count($order->Cards) > 0)
                {
                    $res["id"]           = $order->id;
                    $res["user_id"]      = $order->user_id;
                    $res["shop_id"]      = $order->shop_id;
                    $product             = $order->Cards()->first()->Size->Product;
                    $res["product_name"] = $lang == "ar" ? $product->name_ar:$product->name_en;
                    $res["product_image"]=URL::to('dashboard/uploads/products') . '/' . $product['image'];
                    $res["total price"]  = array_sum($order->Cards->pluck('total')->toArray()) + $order['delivery_coast'];
                    $res["payment "]     = $order->Payment;
                    $res["shop_name"]    = $lang == "ar" ? $order->Shop->name_ar:$order->Shop->name_en;
                    $res["order_created"]= $order->created_at->toDateTimeString();
                    $res["order_status"] = $order->status;
                }
                return $res;
            });
            return response()->json(['key'=>'success','value'=>'1','data'=>$data->toArray() ]);


        }else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
}
