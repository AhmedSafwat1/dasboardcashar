<?php

namespace App\Http\Controllers\API;

use App\Shop;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Validator;
use Auth;
use URL;
class ShopController extends Controller
{
    //get all shops availabale
    public function  GetShops(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'user_id'  => 'required|exists:users,id',
            'lang'     =>'required'
        ]);

        if ($validator->passes()) {

            $user            = User::find($request['user_id']);
            $lat = $user->lat;
            $lng = $user->lng;
            if($lat && $lng)
            {
                $lang = $request["lang"];
                $query = "SELECT id ,name_ar ,delivery,delivery_postion,delivery_coast
                     name_en,lat,lng,image,desc_ar,desc_en,start_time,end_time
                    , ( 3959 * acos ( cos ( radians(". $lat .") ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(".  $lng .") ) + sin ( radians(". $lat .") ) * sin( radians( lat ) ) ) ) AS `distance` FROM `shops` ORDER BY distance ASC";
                $shops = \DB::select($query);
                $data =  collect($shops)->map(function($shop) use ($lang){
                    $d = (array)$shop;
                    $d["name"] = $lang == "ar" ? $d["name_ar"] : $d["name_en"];
                    $d["desc"] = $lang == "ar" ? $d["desc_ar"] : $d["desc_en"];
                    unset($d["name_ar"]);unset($d["name_en"]);unset($d["desc_ar"]);unset($d["desc_en"]);
                    $d["image"] = URL::to('dashboard/uploads/shops').'/'.$d['image'];
                    $d["rate"] =\App\Shop::find($shop->id)->Rates->avg("rate");
                    return $d;
                });

                return response()->json(['key'=>'success','value'=>'1','data'=>$data->toArray()]);
            }
            else
            {
                return response()->json(['key' => 'fail','value' => '0', 'msg' => ""]);
            }

        }else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    //search for the shop by name
    public function search(Request $request)
    {

        $validator=Validator::make($request->all(),[
            'lang'    => 'required',
            'search'  => 'required'
        ],[
            'search.required'      => $request['lang'] == 'ar' ? "الاسم  مطلوب للبحث " : "name required for searching",
        ]);

        if ($validator->passes()) {
            $lang  = $request["lang"];
            $q     = $request["search"];
            $shops = Shop::where ( 'name_ar', 'LIKE', '%' . $q . '%' )->orWhere ( 'name_en', 'LIKE', '%' . $q . '%' )->get();
            $data  = $shops->map(function ($shop) use($lang){
                $d = $shop->toArray();
                $d["name"] = $lang == "ar" ? $d["name_ar"] : $d["name_en"];
                $d["desc"] = $lang == "ar" ? $d["desc_ar"] : $d["desc_en"];
                unset($d["name_ar"]);unset($d["name_en"]);unset($d["desc_ar"]);unset($d["desc_en"]);
                $d["image"] = URL::to('dashboard/uploads/shops').'/'.$d['image'];
                $rate = $shop->Rates->avg("rate");
                $d["rate"] =is_null($rate)?0:$rate;
                return $d;

            });
            return response()->json(['key'=>'success','value'=>'1','data'=>$data->toArray()]);

        }else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    // getShop info
    public function getShop(Request $request)
    {

        $validator=Validator::make($request->all(),[
            'lang'    => 'required',
            'shop_id'  => 'required|exists:shops,id',
            'user_id'  => 'required|exists:users,id'
        ],[
            'shop_id.exists'      => $request['lang'] == 'ar' ? "المطعم غير  موجود  " : "shop  not found ",
        ]);

        if ($validator->passes()) {
            $lang  = $request["lang"];
            $shop  = Shop::find($request["shop_id"]);
            $user  = User::find($request["user_id"]);
            $arr["shop"] = collect($shop->toArray())->only([
                "id",
                "lat","delivery","lng","delivery_coast","delivery_postion",
                "start_time","end_time"
            ])->all();
            $arr["shop"]["name"]            = $request["lang"] == "ar" ? $shop["name_ar"] : $shop["name_en"];
            $arr["shop"]["desc"]            = $request["lang"] == "ar" ? $shop["desc_ar"] : $shop["desc_en"];
            $arr["shop"]["image"]           = URL::to('dashboard/uploads/shops').'/'.$shop['image'];
            $rate                           = $shop->Rates->avg("rate");
            $arr["shop"]["rate"]            = is_null($rate)?0:$rate;
            $arr["shop"]["distance"]        = distance($shop["lat"],$shop["lng"],$user,"K");
            $activ = $shop->Activities->map(function ($act) use($lang){
                $res["id"]   = $act["id"];
                $res["name"] = $lang == "ar" ? $act["name_ar"]: $act["name_en"];
                return $res;
            });
            $arr["shop"]["activities"]            = $activ->toArray();

            $props = $shop->ShopProperties->map(function ($prop) use($lang){
                $res["id"]       = $prop["id"];
                $res["name"]     = $lang == "ar" ? $prop->Property["name_ar"]: $prop->Property["name_en"];
                $res["status"]   = $prop->status;
                return $res;
            });
            $arr["shop"]["properties"]            = $props->toArray();
            $cat = $shop->Categories->map(function ($cat) use($lang){
                $res["id"]   = $cat["id"];
                $res["icon"] = URL::to('dashboard/uploads/categories').'/'.$cat["icon"];
                $res["name"] = $lang == "ar" ? $cat["name_ar"]: $cat["name_en"];
                return $res;
            });
            $arr["categories"]  = $cat->toArray();
            $rats = $shop->Rates->map(function ($rate)  use($lang){
                $res["id"]          = $rate["id"];
                $res["rate"]        = $rate["rate"];
                $res["comment"]     = $rate["comment"];
                $res["created_at"]  = $rate["created_at"]->format('m/d/Y');
                $res["user"]        = collect($rate->User->toArray())->only("name","last","id")->all();
                return $res;
            });
            $arr["rates"]  = $rats->toArray();
            return response()->json(['key'=>'success','value'=>'1','data'=>$arr]);

        }else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    //get shop product based the category
    public function shopProduct(Request $request)
    {

        $validator=Validator::make($request->all(),[
            'lang'        => 'required',
            'category_id' => 'required|exists:categories,id',
            'shop_id'     => 'required|exists:shops,id'
        ],[
            'shop_id.exists'          => $request['lang'] == 'ar' ? "المطعم غير  موجود  " : "shop  not found ",
            'category_id.exists'      => $request['lang'] == 'ar' ? "الصنف غير  موجود  " : "categories  not found ",
        ]);

        if ($validator->passes()) {

            $shop     =  Shop::find($request['shop_id']);
            $category = $shop->Categories()->wherePivot('category_id',"=",$request["category_id"])->first();
            if($category)
            {
                $lang = $request["lang"];
                $products = $category->Products;
                $data =  $products->map(function ($product) use($lang){
                    $res = [];
                    if(count($product->Sizes)>0)
                    {
                        $res["id"]     = $product["id"];
                        $res["shop_id"]= $product["shop_id"];
                        $res["name"]   = $lang == "ar" ? $product["name_ar"]: $product["name_en"];
                        $res["desc"]   = $lang == "ar" ? $product["desc_ar"]: $product["desc_en"];
                        $res["price"]  = count($product->Sizes)  == 1 ?  $product->Sizes()->first()->price : 0 ;
                        $res["image"]  =  URL::to('dashboard/uploads/products').'/'.$product['image'];
                    }
                    return $res;

                });

                $data = array_filter(array_map('myFilter', $data->toArray()));
                return response()->json(['key'=>'success','value'=>'1','data'=>$data]);
            }
            else
            {
                $msg = $request["lang"] == "ar" ? "المحل لايحتوى على هذا الصنف":"shop not have the category";
                return response()->json(['key' => 'fail','value' => '0', 'msg' =>$msg]);
            }



        }else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    //get shop product info based id
    public function ProductInnfo(Request $request)
    {
//        dd($request->all());

        $validator=Validator::make($request->all(),[
            'lang'        => 'required',
            'product_id'  => 'required|exists:products,id',
            'shop_id'     => 'required|exists:shops,id'
        ],[
            'shop_id.exists'          => $request['lang'] == 'ar' ? "المطعم غير  موجود  " : "shop  not found ",
            'product_id.exists'      => $request['lang'] == 'ar' ? "المنتج غير  موجود  " : "product  not found ",
        ]);

        if ($validator->passes()) {

            $shop     =  Shop::find($request['shop_id']);
            $product  = $shop->products()->where('id',"=",$request["product_id"])->first();
            if($product)
            {
                $lang           = $request["lang"];
                $res["id"]      = $product["id"];
                $res["shop_id"] = $product["shop_id"];
                $res["name"]    = $lang == "ar" ? $product["name_ar"]: $product["name_en"];
                $res["desc"]    = $lang == "ar" ? $product["desc_ar"]: $product["desc_en"];
                $res["image"]   =  URL::to('dashboard/uploads/products').'/'.$product['image'];
                $data['product']= $res;
                $add = $product->Addtions->map(function ($addation) use($lang){
                    $res         = collect($addation->toArray())->only(["id","price","quantity","note"])->all();
                    $res["name"] = $lang == "ar" ? $addation["name_ar"]: $addation["name_en"];
                    return $res;

                });
                $data["addations"] = $add->toArray();
                $size = $product->Sizes()->select(["id","size","price","quantity"])->get();
                $data["addations"] = $add->toArray();
                $data["sizes"] = $size->toArray();

                return response()->json(['key'=>'success','value'=>'1','data'=>$data]);
            }
            else
            {
                $msg = $request["lang"] == "ar" ? "المنتج لايحتوى على هذا الصنف":"shop not have the product";
                return response()->json(['key' => 'fail','value' => '0', 'msg' =>$msg]);
            }



        }else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }


}
