<?php

namespace App\Http\Controllers\API;

use App\Bank;
use App\City;
use App\MoneyAccount;
use App\MoneyTransfers;
use App\Product;
use App\Shop;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Validator;
use Auth;
use URL;
use Image;
class BasicInfoController extends Controller
{

    // get all cities in my country
    public function GetCities(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'lang'=>'required'
        ]);

        if ($validator->passes()) {
            $lang    = $request['lang'];
            $cities  = City::all();
            $arr     = [];
            foreach ($cities as $city) {
                $arr[]=[
                    'id'         => $city['id'],
                    'name'       => $request['lang'] == 'ar' ? $city['name_ar'] :$city['name_en']
                ];
            }
            return response()->json(['key'=>'success','value'=>'1','data'=>$arr]);
        }else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    // get all accounts in my country
    public function GetAccounts(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'lang'=>'required'
        ]);

        if ($validator->passes()) {
            $lang    = $request['lang'];
            $accounts  = Bank::all();
            $arr     = [];
            foreach ($accounts as $city) {
                $arr[]=[
                    'id'         => $city['id'],
                    'name'       => $request['lang'] == 'ar' ? $city['name_ar'] :$city['name_en'],
                    "account"    => $city["account"],
                    "iban"       => $city["iban"],
                    "icon"       => URL::to('dashboard/uploads/banks').'/'.$city["iban"],
                ];
            }
            return response()->json(['key'=>'success','value'=>'1','data'=>$arr]);
        }else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    // transfer money banking
    public  function  moneyTransfer(Request $request)
    {
        $niceError = array(
            'account_name.required'       => $request['lang'] == 'ar' ?"اسم صاحب الحساب مطلوب":"account name required",
            'account_name.min'            => $request['lang'] == 'ar' ?"اسم صاحب الحساب لايقل عن حرفين":"account name must be least two characters",
            'account_name.max'            => $request['lang'] == 'ar' ? "اسم صاحب الحساب لايزيد عن 190 حرف" :" account name not increase then 191 characters",
            'account_number.required'     => $request['lang'] == 'ar' ?  "رقم الحساب   مطلوب":"account number required",
            'money.required'              => $request['lang'] == 'ar' ? "المبلغ   مطلوب" :"money required",
            'account_number.numeric'      => $request['lang'] == 'ar' ? "رقم الحساب   يجب ان يكون رقم ":"account number must be numbers",
            "image.required"              => $request['lang'] == 'ar' ?"الصوره مطلوبه":"image required",
            "image.image"                 => $request['lang'] == 'ar' ? "الصوره مطلوبه يجب ان تكون صوره":"image must be image",
            "user_id.exists"              => $request['lang'] == 'ar' ? "المستخدم غير موجود" : "user not found"

        );
        $validator = Validator::make($request->all(),[
            'lang'                   => 'required',
            'account_name'           =>'required|min:2|max:190',
            'account_number'         =>"numeric|required",
            'money'                  =>"numeric|required",
            'image'                  =>'required|image',
            'user_id'                => 'required|exists:users,id',
        ], $niceError);
        if ($validator->passes()) {
            $money                 = new MoneyTransfers;
            $money->account_name   = $request["account_name"];
            $money->account_number = $request["account_number"];
            $money->price          = $request["money"];
            $money->user_id        = $request["user_id"];
            $photo=$request->image;
            $name=date('d-m-y').time().rand().'.'.$photo->getClientOriginalExtension();
            Image::make($photo)->save('dashboard/uploads/money_transfers/'.$name);
            $money->image          =$name;
            $money->type           = 1;
            $money->save();
            $msg =  $request['lang'] == 'ar' ? "تم حفظ التحويل البنكى وفى انتظار المراجعه والموافقه":"saved the bank transfer and wait reversion and confirm";
            return response()->json(['key'=>'success','value'=>'1',"msg"=>$msg]);
        }else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    // get all product offers
    //get shop product based the category
    public function shopsProduct(Request $request)
    {

        $validator=Validator::make($request->all(),[
            'lang'        => 'required',
        ]);
//        dd("sd");
        if ($validator->passes()) {
            $lang = $request["lang"];
            $products = Product::all();
//            dd($products);
            $data =  $products->map(function ($product) use($lang){
                $res = [];
                if(count($product->Sizes)>0)
                {
                    $res["id"]       = $product["id"];
                    $res["shop_id"]  = $product["shop_id"];
                    $res["name"]     = $lang == "ar" ? $product["name_ar"]: $product["name_en"];
                    $res["desc"]     = $lang == "ar" ? $product["desc_ar"]: $product["desc_en"];
                    $res["price"]    = count($product->Sizes)  == 1 ?  $product->Sizes()->first()->price : 0 ;
                    $res["image"]    = URL::to('dashboard/uploads/products').'/'.$product['image'];
                    $rate            = $product->Shop->Rates->avg("rate");
                    $res["shop_rate"]=is_null($rate)?0:$rate;
                    $res["shop_name"]=$lang == "ar" ? $product->Shop->name_ar:$product->Shop->name_en;
                }
                return $res;

            });
//            dd($data->toArray());


            $data = array_filter(array_map('myFilter', $data->toArray()));
            return response()->json(['key'=>'success','value'=>'1','data'=>$data]);

        }else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }

}
