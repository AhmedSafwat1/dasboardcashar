<?php

namespace App\Http\Controllers\API;

use App\Favourit;
use App\Rate;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notification;
use Session;
use Validator;
use Auth;
use URL;
class UserController extends Controller
{
    // add new user
    public  function signUp(Request $request)
    {

        $niceError = array      (
            'lng.required'               => $request['lang'] == 'ar' ? " العنوان مطلوب يجيب اختياره من على الخريطه " : "address is required must chose from map",
            'address.required'           => $request['lang'] == 'ar' ? " العنوان مطلوب يجيب اختياره من على الخريطه " : "address is required must chose from map" ,
            "code.required"              => $request['lang'] == 'ar' ? "كود البلد مطلوب":"country code required",
            "code.exists"                => $request['lang'] == 'ar' ? "كود البلد غير موجود":"country code not exist",
            "last.required"              => $request['lang'] == 'ar' ?" الاسم الاخير مطلوب" : "last name required",
            "birth.required"             => $request['lang'] == 'ar' ? "تاريخ الميلاد مطلوب" : "birth date required",
            'phone.digits_between'       => $request['lang'] == 'ar' ? '  رقم الجوال يجب ان يكون  5 ارقام على الاقل او 40 على الاكثر':"phone number must be 5 number at least or 40 number maxiamum",
            'email.unique'               => $request['lang'] == 'ar' ?'البريد الالكترونى مستخدم من قبل':"email already used to another user" ,
            'password.min'               => $request['lang'] == 'ar' ? 'يجب ان يكون كلمة السر 6 حروف' : 'Password must be 6 characters',
            'password.required'          => $request['lang'] == 'ar' ? 'يجب ادخال كلمه المرور الجديده بشكل صحيح' : 'You must enter the new password correctly',
        );
//        dd($request->all());
        $validator = Validator::make($request->all(),[
            'lang'     => 'required',
            'name'     =>'required|min:2|max:190',
            'email'    =>'nullable|email|min:2|unique:users,email',
            'phone'    =>'required|digits_between:5,40',
            'password' =>'required|min:6|max:190',
            'address'  => 'required',
            'lng'      => 'required',
            'lat'      => 'required',
            'address'  => 'required',
            'last'     => 'required',
            'birth'    => 'required',
            'code'     => "required|exists:cities,id"
        ], $niceError);

        if ($validator->passes())
        {
            if(CheckMobileUnique($request['phone'],$request['code'] )) {
                $user = new User();
                $user['name'] = $request['name'];
                if($request["email"])
                {
                    $user['email'] = $request['email'];
                }
                $user['last']     = $request['last'];
                $user['phone']    = $request['phone'];
                $user['device_id']= $request['device_id'];
                $user['password'] = bcrypt($request['password']);
                $user['code']     = mt_rand(1000, 9999);
                $user['city_id']  = $request['code'];
                $user['birth']    = $request['birth'];
                $user['lat']      = $request['lat'];
                $user['lng']      = $request['lng'];
                $user['address']  = $request['address'];
                $user['active']   = 1;
                $user['confirm']  = 0;
                $user->save();

                // Sent Activation Code To New User
                $msg = $request['lang'] == 'ar' ? 'كود التسجيل الخاص بك : ' . $user['code'] : 'The Registration code is : ' . $user['code'];
                send_mobile_sms($request['phone'], $msg);
                $user = User::find($user['id']);
                $arr['id'] = $user['id'];
                $arr['name'] = $user['name'];
                $arr['last'] = $user['last'];
                $arr['birth'] = $user['birth'];
                $arr['phone'] = $user['phone'];
                $arr['avatar'] = URL::to('dashboard/uploads/users') . '/' . $user['avatar'];
                $arr['confirm'] = (string)$user['confirm'];

                $arr['active'] = (string)$user['active'];
                return response()->json(['key' => 'success', 'value' => '1', 'data' => $arr, 'msg' => '', 'code' => $user['code']]);
            }
            else
            {
                $msg = $request['lang'] == 'ar' ? 'رقم الجوال مستخدم من قبل':"phone already used to another user";
                return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg]);
            }
        }else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }

    }
    // check code
    public function Check_Code(Request $request)
    {

        $validator=Validator::make($request->all(),[
            'lang'    => 'required',
            'user_id' => 'required|exists:users,id',
            'code'    => 'required'
        ],[
            'code.required'      => $request['lang'] == 'ar' ? "الكود مطلوب " : "code required",
        ]);

        if ($validator->passes()) {

            $user  =  User::find($request['user_id']);

            if($user['code'] == $request['code']){
                $user['confirm'] = 1;
                $user->save();
                $msg = $request['lang']=='ar' ?'تم تنشيط الحساب بنجاح' : 'Account successfully activated';
                return response()->json(['key'=>'success', "user_id"=>$request['user_id'], 'value'=>'1','msg'=>$msg]);
            }else{
                $msg = $request['lang']=='ar' ?'كود التحقق غير صحيج' : 'Verification code is not valid.';
                return response()->json(['key'=>'fail','value'=>'0','msg'=>$msg]);
            }

        }else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    //forget password
    public function forgotPassword(Request $request)
    {

        $validator=Validator::make($request->all(),[
            'lang'     => 'required',
            'code'     => "required|exists:cities,id",
            'phone'    => 'required|exists:users,phone',

        ],[
            "code.required"              => $request['lang'] == 'ar' ? "كود البلد مطلوب":"country code required",
            "code.exists"                => $request['lang'] == 'ar' ? "كود البلد غير موجود":"country code not exist",
            'phone.exists'               => $request['lang'] == 'ar' ? 'رقم الجوال غير مسجل لدينا' : 'Mobile number is not registered',
            "phone.required"             => $request['lang'] == 'ar' ? "رقم الجوال مطلوب":"Mobile number is required",
        ]);

        if ($validator->passes()) {
            $user         = User::where('phone',$request['phone'])
                                ->where('city_id',$request['code'])
                                ->first();
            $code         = mt_rand(1000, 9999);
            $user['code'] = $code;
            $user->save();

            if($request['lang'] == 'ar'){
                $msg = 'كود استرجاع كلمه المرور الخاص بك هو : '.$code;
            }else{
                $msg = 'The verification code is : '.$code;
            }

            send_mobile_sms($request['phone'],$msg);

            return response()->json(['key'=>'success','value'=>'1','code'=>$code ,'user_id'=>$user['id']]);

        }else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    //resend the code
    public function ResendCode(Request $request)
    {
        $validator=Validator::make($request->all(),[

            'user_id'   => 'required|exists:users,id',

        ],[
            'user_id.exists'               => $request['lang'] == 'ar' ? 'المستخدم غير مسجل لدينا' : 'user is not registered',
//            "user_id.required"             => $request['lang'] == 'ar' ? "المستخدم مطلوب":"user is required",
        ]);

        if ($validator->passes()) {
            $user         = User::find($request["user_id"]);
            $code         = mt_rand(1000, 9999);
            $user['code'] = $code;
            $user->save();

            if($request['lang'] == 'ar'){
                $msg = 'كود استرجاع كلمه المرور الخاص بك هو : '.$code;
            }else{
                $msg = 'The verification code is : '.$code;
            }

            send_mobile_sms($user['phone'],$msg);

            return response()->json(['key'=>'success','value'=>'1','code'=>$code ,'user_id'=>$user['id']]);

        }else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    //reset password
    public function resetPassword(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'lang'      => 'required',
            'user_id'   => 'required|exists:users,id',
            'password'  =>'required|min:6|max:190',
            'code'      =>"required",

        ],[
            'password.min'               => $request['lang'] == 'ar' ? 'يجب ان يكون كلمة السر 6 حروف' : 'Password must be 6 characters',
            'code.required'              => $request['lang'] == 'ar' ? 'الكود مطلوب' : 'code required',
            'password.confirmed'         => $request['lang'] == 'ar' ? 'تاكيد كلمة المرور غير مطابق ' : 'confirm password not the same password',
            ]);

        if ($validator->passes()) {

            $user = User::find($request['user_id']);
            if($user['code'] == $request['code']){
                $user['password'] = bcrypt($request['password']);
                $user->update();

                $msg = $request['lang'] == 'ar' ? ' تم تغيير كلمه المرور بنجاح' :'Your password successfully changed.';
                return response()->json(['key'=>'success','value'=>'1','msg'=>$msg]);
            }else{
                $msg = $request['lang'] == 'ar' ? 'كود التحقق غير صيحيح' :'Verification code is incorrect.';
                return response()->json(['key'=>'fail','value'=>'0','msg'=>$msg]);
            }
        }else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    // login in
    public function signIn(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'lang'      => 'required',
            'phone'     => 'required',
            'code'      => "required|exists:cities,id",
            'device_id' => 'required',
            'password'  => 'required',

        ]);
        if ($validator->passes()) {
            if(!Auth::attempt( ['phone'=>$request['phone'],'city_id'=>$request['code'],'password' =>$request['password']])){
                $msg = $request['lang'] == 'ar' ? 'رقم الجوال أو كلمه المرور غير صحيحه.' : 'The phone number or password is wrong.';
                return response()->json(['key'=>'fail','value'=>'0','msg'=>$msg]);
            }
            $user = User::where('phone',$request['phone'])
                        ->where('city_id',$request['code'])
                        ->first();
            if($user == 0)
            {
                $msg = $request['lang'] == 'ar' ? 'المستخدم لم يقم بتاكديد رقم الموبيل.' : ' user not verification phone  .';
                return response()->json(['key'=>'fail','value'=>'0','msg'=>$msg]);
            }
            $user['device_id']   = $request['device_id'];
            $user->update();
            $arr['id']           = $user['id'];
            $arr['name']         = $user['name'];
            $arr['last']         = $user['last'];
            $arr['birth']        = $user['birth'];
            $arr['phone']        = $user['phone'];
            $arr['avatar']       = URL::to('dashboard/uploads/users').'/'.$user['avatar'];
            $arr['confirm']      = (string)$user['confirm'];

            $arr['active']       = (string)$user['active'];


            return response()->json(['key'=>'success','value'=>'1','data'=>$arr, 'msg' => '' ]);

        }else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    // get user id
    public function GetUser(Request $request)
    {

        $validator=Validator::make($request->all(),[
            'lang'    => 'required',
            'user_id' => 'required|exists:users,id',

        ],[
            "user_id.exists" =>$request['lang'] == 'ar' ? "المستخدم غير موجود" : "user not found"
            ]);

        if ($validator->passes()) {

            $user  =  User::find($request['user_id']);
            $arr['id']           = $user['id'];
            $arr['name']         = $user['name'];
            $arr['last']         = $user['last'];
            $arr['birth']        = $user['birth'];
            $arr['phone']        = $user['phone'];
            $arr['code']         = $user['city_id'];
            $arr['avatar']       = URL::to('dashboard/uploads/users').'/'.$user['avatar'];
            $arr['confirm']      = (string)$user['confirm'];
            $arr['notification'] = (string)$user['notification'];
            $arr['active']       = (string)$user['active'];
            return response()->json(['key'=>'success','value'=>'1','data'=>$arr ,'code' => $user['code']]);

        }else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    //update user profile
    public  function updateProfile(Request $request)
    {

        $niceError = array      (
            'lng.required'               => $request['lang'] == 'ar' ? " العنوان مطلوب يجيب اختياره من على الخريطه " : "address is required must chose from map",
            'address.required'           => $request['lang'] == 'ar' ? " العنوان مطلوب يجيب اختياره من على الخريطه " : "address is required must chose from map" ,
            "code.required"              => $request['lang'] == 'ar' ? "كود البلد مطلوب":"country code required",
            "code.exists"                => $request['lang'] == 'ar' ? "كود البلد غير موجود":"country code not exist",
            "last.required"              => $request['lang'] == 'ar' ?" الاسم الاخير مطلوب" : "last name required",
            "birth.required"             => $request['lang'] == 'ar' ? "تاريخ الميلاد مطلوب" : "birth date required",
            'phone.digits_between'       => $request['lang'] == 'ar' ? '  رقم الجوال يجب ان يكون  5 ارقام على الاقل او 40 على الاكثر':"phone number must be 5 number at least or 40 number maxiamum",

            'email.unique'               => $request['lang'] == 'ar' ?'البريد الالكترونى مستخدم من قبل':"email already used to another user" ,

            "user_id.exists"             => $request['lang'] == 'ar' ? "المستخدم غير موجود" : "user not found"
            );

        $validator = Validator::make($request->all(),[
            'lang'     => 'required',
            'user_id' => 'required|exists:users,id',
            'name'     =>'required|min:2|max:190',
            'email'    =>'nullable|email|min:2|unique:users,email',
            'phone'    =>'required|digits_between:5,40',

            'address'  => 'required',
            'lng'      => 'required',
            'last'     => 'required',
            'birth'    => 'required',
            'code'     => "required|exists:cities,id",

        ], $niceError);

        if ($validator->passes())
        {
            if(CheckMobileUnique($request['phone'],$request['code'],$request['user_id'] )) {
                $user = User::find($request['user_id']);
                $user['name'] = $request['name'];
                $user['email'] = $request['email'];
                $user['last'] = $request['last'];
                $user['phone'] = $request['phone'];
                $user['device_id'] = $request['device_id'];
                $user['password'] = bcrypt($request['password']);
                $user['city_id'] = $request['code'];
                $user['birth']  = $request['birth'];
                $user['lat']    = $request['lat'];
                $user['lng']    = $request['lng'];
                $user['address']= $request['address'];
                $user->save();
                $arr['id']     = $user['id'];
                $arr['name']   = $user['name'];
                $arr['last']   = $user['last'];
                $arr['phone']  = $user['phone'];

                $arr['avatar'] = URL::to('dashboard/uploads/users') . '/' . $user['avatar'];
                $arr['confirm']= (string)$user['confirm'];
                $arr['active'] = (string)$user['active'];
                return response()->json(['key' => 'success', 'value' => '1', 'data' => $arr, 'msg' => '']);
             }else
                {
                    $msg = $request['lang'] == 'ar' ? 'رقم الجوال مستخدم من قبل':"phone already used to another user";
                    return response()->json(['key'=>'fail','value'=>'0','msg'=>$msg]);
                }
            }else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }

    }
    //change user password
    public  function changePassword(Request $request)
    {

        $niceError = array(
            'password.required'                  => $request['lang'] == 'ar' ? 'يجب ادخال كلمه المرور الجديده بشكل صحيح' : 'You must enter the new password correctly',
            'email.unique'                       => $request['lang'] == 'ar' ?'البريد الالكترونى مستخدم من قبل':"email already used to another user" ,
            'password.min'                       => $request['lang'] == 'ar' ? 'يجب ان يكون كلمة السر 6 حروف' : 'Password must be 6 characters',
            'old_password.required'              => $request['lang'] == 'ar' ? 'يجب ادخال كلمه المرور القديمه بشكل صحيح' : 'You must enter the old password correctly',
            'old_password.min'                   => $request['lang'] == 'ar' ? 'كلمه المرور القديمه يجب ان تكون علي الاقل ٦ حروف' : 'The old password must be at least 6 letters long',
            "user_id.exists"                     => $request['lang'] == 'ar' ? "المستخدم غير موجود" : "user not found"
        );

        $validator = Validator::make($request->all(),[
            'lang'                      => 'required',
            'user_id'                   =>'required|exists:users,id',
            'password'                  =>'required|min:6|max:190',
            'old_password'              =>'required|min:6|max:190',

        ], $niceError);

        if ($validator->passes())
        {
            $user = User::find($request['user_id']);
            if(\Hash::check($request['old_password'],$user->password)) {
                $user['password'] = bcrypt($request['password']);
                $user->save();
                $arr['id'] = $user['id'];
                $arr['name'] = $user['name'];
                $arr['last'] = $user['last'];
                $arr['phone'] = $user['phone'];
                $arr['avatar'] = URL::to('dashboard/uploads/users') . '/' . $user['avatar'];
                $arr['confirm'] = (string)$user['confirm'];
                $arr['active'] = (string)$user['active'];
                return response()->json(['key' => 'success', 'value' => '1', 'data' => $arr, 'msg' => '']);
            }else
            {
                $msg = $request['lang'] == 'ar' ? 'رقم السرى القديم غير صحيح ':"old password not correct";
                return response()->json(['key'=>'fail','value'=>'0','msg'=>$msg]);
            }
        }else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }

    }
    //home page
    public function homeUser(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'lang'     =>'required',
            'user_id'  => 'required|exists:users,id',

        ]);

        if ($validator->passes()) {

            $user            = User::find($request['user_id']);
            $lat = $user->lat;
            $lng = $user->lng;
            if($lat && $lng)
            {
                $lang = $request["lang"];
                $query = "SELECT id ,name_ar ,delivery,delivery_postion,delivery_coast
                     name_en,lat,lng,image,desc_ar,desc_en,start_time,end_time
                    , ( 3959 * acos ( cos ( radians(". $lat .") ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(".  $lng .") ) + sin ( radians(". $lat .") ) * sin( radians( lat ) ) ) ) AS `distance` FROM `shops` ORDER BY distance ASC";
                $shops = \DB::select($query);
                $data =  collect($shops)->map(function($shop) use ($lang){
                    $d = (array)$shop;
                    $d["name"] = $lang == "ar" ? $d["name_ar"] : $d["name_en"];
                    $d["desc"] = $lang == "ar" ? $d["desc_ar"] : $d["desc_en"];
                    unset($d["name_ar"]);unset($d["name_en"]);unset($d["desc_ar"]);unset($d["desc_en"]);
                    $d["image"] = URL::to('dashboard/uploads/shops').'/'.$d['image'];
                    $rate = \App\Shop::find($shop->id)->Rates->avg("rate");
                    $d["rate"] = is_null($rate)?0:$rate;
                    return $d;
                });

                return response()->json(['key'=>'success','value'=>'1','data'=>$data->toArray()]);
            }
            else
            {
                $msg  = $request["lang"] == "ar"? "لانستطيع تحديد مكانك" : "can't locate the postion";
                return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg]);
            }

        }else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    //user favourit
    public function userFavourit(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'lang'     =>'required',
            'user_id'  => 'required|exists:users,id',

        ]);

        if ($validator->passes()) {

            $user       = User::find($request['user_id']);
            $favourits  = $user->Favourits()->with("product")->get();
            $favourits->makeHidden("product_id");
            $lang = $request["lang"];
            $data = ($favourits)->map(function($fav) use ($lang){
                $dd["product"]          = $fav->Product->toArray();
                $dd["Shop"]             = $fav->Product->Shop->toArray();
                $dd["shopRate"]         = $fav->Product->Shop->Rates->avg("rate");
                $dd["Shop"]["name"]     = $lang == "ar" ? $dd["Shop"]["name_ar"] : $dd["Shop"]["name_en"];
                $dd["Shop"]["desc"]     = $lang == "ar" ? $dd["Shop"]["desc_ar"] : $dd["Shop"]["desc_en"];
                $dd["product"]["name"]  = $lang == "ar" ? $dd["product"]["name_ar"] : $dd["product"]["name_en"];
                $dd["product"]["desc"]  = $lang == "ar" ? $dd["product"]["desc_ar"] : $dd["product"]["desc_en"];
                $dd["Shop"]["image"]    = URL::to('dashboard/uploads/shops').'/'.$dd["Shop"]['image'];
                $dd["product"]["image"] = URL::to('dashboard/uploads/products').'/'.$dd["product"]['image'];
                $dd["product_price"]    = $fav->Product->Sizes()->first()->price;
                unset($dd["Shop"]["name_ar"],$dd["Shop"]["name_en"]
                      ,$dd["Shop"]["desc_ar"], $dd["Shop"]["desc_en"]);
                unset($dd["product"]["name_ar"],$dd["product"]["name_en"]
                    ,$dd["product"]["desc_ar"], $dd["product"]["desc_en"]);

                return $dd;
            });

            return response()->json(['key'=>'success','value'=>'1','data'=>$data->toArray()]);


        }else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    // add and reomve from favourit
    public function postFavourite(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'product_id'	=> 'required|exists:products,id',
            'user_id'       => 'required|exists:users,id',
            'lang'          => 'required'
        ]);

        if ($validator->passes()) {

            $favourites = Favourit::where('user_id',$request['user_id'])->where('product_id',$request['product_id'])->first();

            if($favourites){
                $favourites->delete();
                $msg = $request['lang']=='ar' ? 'تم الحذف من المفضله.' : 'You disfavourated this service.';
                return response()->json(['key'=>'success','value'=>'1','status' => 'unlike' ,'msg'=>$msg]);
            }
            $favourite                 = new Favourit();
            $favourite['product_id'] = $request['product_id'];
            $favourite['user_id']     = $request['user_id'];
            $favourite->save();
            $msg = $request['lang']=='ar' ? 'تم الاضافه الي المفضله.' : 'You favourated this service.';
            return response()->json(['key'=>'success','value'=>'1','status' => 'like' ,'msg'=>$msg
            ]);

        }else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
    // add rate
    public function postRate(Request $request)
    {
        $validator=Validator::make($request->all(),[
            'lang'          => 'required',
            'shop_id'       => 'required|exists:shops,id',
            'user_id'       => 'required|exists:users,id',
            'rate'          => 'required',
        ]);

        if ($validator->passes()) {
            $rate = Rate::where('shop_id',$request['shop_id'])->where('user_id',$request['user_id'])->orderBy('created_at','desc')->first();
            if($rate){
                $rate['rate'] = $request['rate'];
                if(isset( $request['comment']))  $rate['comment'] = $request['comment'];
                $rate->update();
                $msg =  $request['lang']=='ar' ? 'تم تحديث التقييم بنجاح.' : 'Your rate updated successfully.';
                return response()->json(['key'=>'success', 'value'=>'1', 'msg'=>$msg]);
            }

            $rate                 = new Rate();
            $rate['shop_id']      = $request['shop_id'];
            $rate['user_id']      = $request['user_id'];
            $rate['rate']         = $request['rate'];
            if(isset( $request['comment']))  $rate['comment'] = $request['comment'];
            $rate->save();

            $msg =  $request['lang']=='ar' ? 'تم التقييم بنجاح.' : 'This service successfully rated.';
            return response()->json(['key'=>'success', 'value'=>'1', 'msg'=>$msg]);


        }else{
            foreach ((array)$validator->errors() as $key => $value){
                foreach ($value as $msg){
                    return response()->json(['key' => 'fail','value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }
}
