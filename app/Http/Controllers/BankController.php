<?php

namespace App\Http\Controllers;

use App\Bank;
use Illuminate\Http\Request;
use Auth;
use Session;
use Image;
use File;

class BankController extends Controller
{
    //
    public function  __construct()
    {
        \Carbon::setLocale('ar');
    }
    // display banks
    public function banks()
    {
        $banks   = Bank::all();
       
        return view('dashboard.banks.banks',compact('banks'));
    }
     // add banks
     public function add(Request $request)
     {
        
         
         $niceError = array(
            'name_ar.required'       => "اسم البنك باللغه العربيه مطلوب", 
            'name_en.required'       => "اسم البنك باللغه الانجليزىه  مطلوب",
            'name_ar.min'            => "اسم البنك باللغه العربيه لايقل عن حرفين", 
            'name_en.min'            => "اسم البنك باللغه الانجليزىه  لايقل عن حرفين",
            'name_ar.max'            => "اسم البنك باللغه العربيه لايزيد عن 190 حرف", 
            'name_en.max'            => "اسم البنك باللغه الانجليزىه  لايزيد عن  190 جرف",
            'account.required'       => "رقم الحساب   مطلوب",
            'iban.required'          => "رقم الابيان   مطلوب",
            'account.numeric'        => "رقم الحساب   يجب ان يكون رقم ",
            'iban.numeric'           => "رقم الابيان يجب ان يكون رقم  ",
            "icon.required"          => "الصوره مطلوبه",
            "icon.image"             => "الصوره مطلوبه يجب ان تكون صوره",

        );
        $this->validate($request,[
            'name_ar'           =>'required|min:2|max:190',
            'name_en'           =>'required|min:2|max:190',
            'account'           =>"numeric|required",
            'iban'              =>"numeric|required",
            'icon'             =>'required|image',
        ], $niceError);
        $bank                = new  Bank;
        $bank->name_ar       =$request->name_ar;
        $bank->name_en       =$request->name_en;
        $bank->account       =$request->account;
        $bank->iban          =$request->iban;
        $photo=$request->icon;
        $name=date('d-m-y').time().rand().'.'.$photo->getClientOriginalExtension();
        Image::make($photo)->save('dashboard/uploads/banks/'.$name);
        $bank->icon          =$name;
        $bank->save();
        Report(Auth::user()->id,'بأضافة حساب جديد');
        Session::flash('success','تم اضافة حساب');
        // dd($bank);
        return back();
     }
     // edit banks
     public function edit(Request $request)
     {
        
         
         $niceError = array(
            'edit_name_ar.required'       => "اسم البنك باللغه العربيه مطلوب", 
            'edit_name_en.required'       => "اسم البنك باللغه الانجليزىه  مطلوب",
            'edit_name_ar.min'            => "اسم البنك باللغه العربيه لايقل عن حرفين", 
            'edit_name_en.min'            => "اسم البنك باللغه الانجليزىه  لايقل عن حرفين",
            'edit_name_ar.max'            => "اسم البنك باللغه العربيه لايزيد عن 190 حرف", 
            'edit_name_en.max'            => "اسم البنك باللغه الانجليزىه  لايزيد عن  190 جرف",
            'edit_account.required'       => "رقم الحساب   مطلوب",
            'edit_iban.required'          => "رقم الابيان   مطلوب",
            'edit_account.numeric'        => "رقم الحساب   يجب ان يكون رقم ",
            'edit_iban.numeric'           => "رقم الابيان يجب ان يكون رقم  ",
            "edit_icon.image"             => "الصوره مطلوبه يجب ان تكون صوره",

        );
        $this->validate($request,[
            'edit_name_ar'           =>'required|min:2|max:190',
            'edit_name_en'           =>'required|min:2|max:190',
            'edit_account'           =>"numeric|required",
            'edit_iban'              =>"numeric|required",
            'edit_icon'              =>'nullable|image',
        ], $niceError);
        $bank                =Bank::findOrFail($request->id);
        $bank->name_ar       =$request->edit_name_ar;
        $bank->name_en       =$request->edit_name_en;
        $bank->account       =$request->edit_account;
        $bank->iban          =$request->edit_iban;
        if(!empty($request->edit_icon))
        {
            $photo=$request->edit_icon;
            $name=date('d-m-y').time().rand().'.'.$photo->getClientOriginalExtension();
            if($bank->icon != 'default.png')
            {
                File::delete('dashboard/uploads/banks/'.$bank->icon);
                Image::make($photo)->save('dashboard/uploads/banks/'.$name);
            }
            else
            {
                Image::make($photo)->save('dashboard/uploads/banks/'.$name);
            }
            $bank->icon=$name;   
        }
        $bank->save();
        Report(Auth::user()->id,'بتحديث بيانات  '.$bank->name_ar);
        Session::flash('success','تم حفظ التعديلات');
        // dd($bank);
        return back();
     }
      // delete bank
    public function delete(Request $request)
    {
        // dd($request->all());
        $bank = Bank::findOrFail($request->id);
        if($bank->image != 'default.png')
             File::delete('dashboard/uploads/banks/'.$bank->image );
        $bank->delete();
        Report(Auth::user()->id,'بحذف الحساب '.$bank->name_ar);
        Session::flash('success','تم الحذف');
        return back();
    }


}
