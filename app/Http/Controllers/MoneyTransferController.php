<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\MoneyTransfers;
use Session;
class MoneyTransferController extends Controller
{
    
     #money accounts page
     public function MoneyAccountsPage()
     {
         $transfers = MoneyTransfers::with('User')->get();
         return view('dashboard.money_transfers.money_transfers',compact('transfers',$transfers));
     }
 
     #accept
     public function Accept(Request $request)
     {
         $user = User::findOrFail($request->id);
         $user->budget = $user->budget + $request->ammount;
         $user->save();
         $transfer = MoneyTransfers::findOrFail($request->money_id);
         $transfer->confirm = 1;
         $transfer->save();
         Session::flash('success','تم تأكيد المعامله');
         return back();
     }
 
     #accept and delete
     public function AcceptAndDelete(Request $request)
     {
         $user = User::findOrFail($request->id);
         $user->budget = $user->budget + $request->ammount;
         $user->save();
         $transfer = MoneyTransfers::findOrFail($request->money_id);
         $transfer->delete();
         Session::flash('success','تم تأكيد المعامله مع الحذف');
         return back();
     }
 
     #delete
     public function Delete(Request $request)
     {
        $transfer = MoneyTransfers::findOrFail($request->money_id);
        $transfer->delete();
        Session::flash('success','تم الحذذف');
        return back();
     }
}
