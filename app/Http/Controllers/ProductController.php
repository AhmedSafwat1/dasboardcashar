<?php

namespace App\Http\Controllers;

use Auth;
use File;
use Image;
use Session;
use App\Shop;
use App\Size;
use App\Addtion;
use App\Product;
use App\Category;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    //all products
    public function products(Request $request)
    {
        $shop        = Shop::findOrFail($request->shop_id);
        $products    = $shop->Products;
        $categories  = Category::all();
        $addtions    = Addtion::all();
        return view('dashboard.products.products',compact('products',"categories" ,"shop","addtions"));
    }
     //all products
     public function add(Request $request)
     {
         $niceError = array(
            "name_ar.required"          => "الاسم باللغه العربيه مطلوب",
            "name_en.required"          => "الاسم باللغه الانجليزيه مطلوب",
            'name_ar.min'               => "اسم المنتج باللغه العربيه لايقل عن حرفين",
            'name_en.min'               => "اسم المنتج باللغه الانجليزىه  لايقل عن حرفين",
            'name_ar.max'               => "اسم المنتج باللغه العربيه لايزيد عن 190 حرف",
            'name_en.max'               => "اسم المنتج باللغه الانجليزىه  لايزيد عن  190 جرف",
            "image.required"            => "الصوره مطلوبه",
            "image.image"               => "الصوره المطلوبه يجب ان تكون صوره",
            'prepared.required'         =>  ' وقت اعداد المنتج مطلوب ',
            'category_id.required'      =>  ' الصنف مطلوب ',
            'shop_id.required'          =>  ' المحل  مطلوب ',
            "category_id.exists"        => " الصنف  غير موجود ",
            "shop_id.exists"            => " الصنف  غير موجود ",
            'prepared.min'              => " وقت الاعداد بالدقائق لايمكن ان يكون بالسالب", 
        );
         $this->validate($request,[
             'name_ar'           =>'required|min:2|max:190',
             'name_en'           =>'required|min:2|max:190',
             'image'             =>'required|image',
             'prepared'          => 'required|numeric|min:0"',
             'category_id'       =>"required|exists:categories,id",
             'shop_id'           =>"required|exists:shops,id",
            
         ], $niceError);
         $shop                   = Shop::findOrFail($request->shop_id);
         $product                = new Product;
         $product->name_ar       =$request->name_ar;
         $product->name_en       =$request->name_en;
         $product->desc_ar       =$request->desc_ar;
         $product->desc_en       =$request->desc_en;
         $product->prepared      =$request->prepared;
         $product->category_id   =$request->category_id;
         $product->shop_id       =$shop->id;
         $photo=$request->image;
         $name=date('d-m-y').time().rand().'.'.$photo->getClientOriginalExtension();
         Image::make($photo)->save('dashboard/uploads/products/'.$name);
         $product->image=$name;
         $product->save();
         Report(Auth::user()->id,'بأضافة منتج جديد ل'.$shop->name_ar);
         Session::flash('success','تم اضافة المنتج');
         // dd($shop);
         return back();
        //  return view('dashboard.products.products',compact('products',"categories" ,"shop"));
     }
    //edit product home
    public function edit(Request $request)
    {
        $shop        = Shop::findOrFail($request->shop_id);
        $product     = $shop->Products()->where("id","=",$request->product_id)->first();
        if(!$product) abort('404');
        $addtions    = $product->Addtions;
        $categories  = Category::all();
        return view('dashboard.products.editProduct',compact("shop","product","addtions","categories"));
    }
    // delete product
    public function delete(Request $request)
    {
        $shop        = Shop::findOrFail($request->shop_id);
        $product     = $shop->Products()->where("id","=",$request->product_id)->first();
        if(!$product) abort('404');;
       
        if($product->image != 'default.png')
             File::delete('dashboard/uploads/products/'.$product->image );
        $product->delete();    
        Report(Auth::user()->id,'بحذف منتج  '.$product->name_ar." ل ".$shop->name_ar);
         Session::flash('success','تم الحذف ');
         // dd($shop);
        return back();
    }
    //save edit product
    public function update(Request $request)
    {
        // dd($request->all());
        $niceError = array(
            "name_ar.required"          => "الاسم باللغه العربيه مطلوب",
            "name_en.required"          => "الاسم باللغه الانجليزيه مطلوب",
            'name_ar.min'               => "اسم المنتج باللغه العربيه لايقل عن حرفين",
            'name_en.min'               => "اسم المنتج باللغه الانجليزىه  لايقل عن حرفين",
            'name_ar.max'               => "اسم المنتج باللغه العربيه لايزيد عن 190 حرف",
            'name_en.max'               => "اسم المنتج باللغه الانجليزىه  لايزيد عن  190 جرف",
            "image.image"               => "الصوره المطلوبه يجب ان تكون صوره",
            'prepared.required'         =>  ' وقت اعداد المنتج مطلوب ',
            'category_id.required'      =>  ' الصنف مطلوب ',
            'shop_id.required'          =>  ' المحل  مطلوب ',
            "category_id.exists"        => " الصنف  غير موجود ",
            "shop_id.exists"            => " الصنف  غير موجود ",
            'prepared.min'              => " وقت الاعداد بالدقائق لايمكن ان يكون بالسالب", 
        );
         $this->validate($request,[
             'name_ar'           =>'required|min:2|max:190',
             'name_en'           =>'required|min:2|max:190',
             'image'             =>'nullable|image',
             'prepared'          => 'required|numeric|min:0"',
             'category_id'       =>"required|exists:categories,id",
             'shop_id'           =>"required|exists:shops,id",
         ], $niceError);
         $shop        = Shop::findOrFail($request->shop_id);
         $product     = $shop->Products()->where("id","=",$request->product_id)->first();
         if(!$product)  abort('404');
         $product->name_ar       =$request->name_ar;
         $product->name_en       =$request->name_en;
         $product->desc_ar       =$request->desc_ar;
         $product->desc_en       =$request->desc_en;
         $product->prepared      =$request->prepared;
         $product->category_id   =$request->category_id;
         if(!empty($request->image))
         {
                $photo=$request->image;
                $name=date('d-m-y').time().rand().'.'.$photo->getClientOriginalExtension();
                if($shop->image != 'default.png')
                {
                    File::delete('dashboard/uploads/products/'.$shop->image);
                    Image::make($photo)->save('dashboard/uploads/products/'.$name);
                }
                else
                {
                    Image::make($photo)->save('dashboard/uploads/products/'.$name);
                }
                $product->image=$name;   
        }
        $product->save();
        Report(Auth::user()->id,'بتعديل منتج  '.$product->name_ar." ل ".$shop->name_ar);
         Session::flash('success','تم حفظ التعديلات');
         // dd($shop);
         return back();

    }
    //save addtions
    public function addation(Request $request)
    {
        // dd($request->all());
        $niceError = array(
            
            'quantity.min'              =>  '  الكميه لاتكون قيمه سالبه ',
            'quantity.required'         =>  '  كمية المنتج مطلوبه ',
            'add_product_id.required'   =>  ' المنتج مطلوب ',
            'add_shop_id.required'      =>  ' المحل  مطلوب ',
            "add_product_id.exists"     => " المنتج  غير موجود ",
            "add_shop_id.exists"        => " المحل  غير موجود ",
            'price.min'                 => " سعر الاضافه لايمكن ان يكون بالسالب", 
            'price.required'            => " سعر  الاضافه مطلوب ",
            'name_ar.required'          => "اسم لاضافه باللغه العربيه مطلوب",
            'name_en.required'          => "اسم لاضافه باللغه الانجليزىه  مطلوب",
            'name_ar.min'               => " اسم لاضافه باللغه العربيه لايقل عن 3 حروف",
            'name_en.min'               => " اسم لاضافه باللغه الانجليزيه لايقل عن 3 حروف",
            'name_ar.max'               => " اسم لاضافه باللغه العربيه لايزيد عن 190 حروف",
            'name_en.max'               => " اسم لاضافه باللغه الانجليزيه  لايزيد عن 190 حروف",
            
        );
         $this->validate($request,[
             'quantity'           =>"required|numeric|min:0",
             "add_product_id"     =>"required|exists:products,id",
             'add_shop_id'        =>"required|exists:shops,id",
             'price'              => "required|numeric|min:0",
             'name_ar'            => 'required|min:3|max:190',
             'name_en'            => 'required|min:3|max:190',
          
         ], $niceError);
        $shop                       = Shop::findOrFail($request->add_shop_id);
        $product                    = $shop->Products()->where("id","=",$request->add_product_id)->first();
        if(!$product) abort('404');
        $checkUnique = Addtion::where("name_ar","=",$request->name_ar)
            ->where("product_id","=",$product->id)
            ->get();
         if(!$checkUnique->isEmpty())
        {
           
            Session::flash('danger','الاسم بالعربى للاضافه موجود مسبقا ف هذا الصنف');
            return back();
        }
        $checkUnique = Addtion::where("name_en","=",$request->name_en)
            ->where("product_id","=",$product->id)
            ->get();
       if(!$checkUnique->isEmpty())
        {
            Session::flash('danger','الاسم لانجليزى للاضافه موجود مسبقا ف هذا الصنف');
            return back();
        }
        $productAddtion             = new Addtion;
        $productAddtion->name_ar    = $request->name_ar;
        $productAddtion ->name_en   = $request->name_en;
        $productAddtion->product_id = $product->id;
        $productAddtion->note       = $request->note;
        $productAddtion->price      = $request->price;
        $productAddtion->quantity   = $request->quantity;
        $productAddtion->save();
        Report(Auth::user()->id,'بأضافة اضافه جديد ل'.$product->name_ar);
        Session::flash('success','تم الاضافة ');
         // dd($shop);
         return back();


    }
    //edit addtion
    //all products
    public function editAddation(Request $request)
    {
        // dd($request->all());
        $niceError = array(
            'edit_quantity.min'              =>  '  الكميه لاتكون قيمه سالبه ',
            'edit_quantity.required'         =>  '  كمية المنتج مطلوبه ',
            'edit_product_id.required'       =>  ' المنتج مطلوب ',
            'edit_shop_id.required'          =>  ' المحل  مطلوب ',
            "edit_product_id.exists"         => " المنتج  غير موجود ",
            "edit_shop_id.exists"            => " المحل  غير موجود ",
            "edit_addtion_id.exists"         => " الاضافه  غير موجود ",
            "edit_addtion_id.required"       =>  ' الاضافه  مطلوب ',
            'edit_price.min'                 => " سعر الاضافه لايمكن ان يكون بالسالب", 
            'edit_price.required'            => " سعر  الاضافه مطلوب ",
            'edit_name_ar.required'          => "اسم لاضافه باللغه العربيه مطلوب",
            'edit_name_en.required'          => "اسم لاضافه باللغه الانجليزىه  مطلوب",
            'edit_name_ar.min'               => " اسم لاضافه باللغه العربيه لايقل عن 3 حروف",
            'edit_name_en.min'               => " اسم لاضافه باللغه الانجليزيه لايقل عن 3 حروف",
            'edit_name_ar.max'               => " اسم لاضافه باللغه العربيه لايزيد عن 190 حروف",
            'edit_name_en.max'               => " اسم لاضافه باللغه الانجليزيه  لايزيد عن 190 حروف",
        );
         $this->validate($request,[
             'edit_quantity'           =>"required|numeric|min:0",
             "edit_product_id"         =>"required|exists:products,id",
             'edit_shop_id'            =>"required|exists:shops,id",
             'edit_price'              => "required|numeric|min:0",
             'edit_name_ar'            => 'required|min:3|max:190',
             'edit_name_en'            => 'required|min:3|max:190',
         ], $niceError);
        $shop                       = Shop::findOrFail($request->edit_shop_id);
        $product                    = $shop->Products()->where("id","=",$request->edit_product_id)->first();
        $productAddtion             = Addtion::findOrFail($request->id);
        if(!$product)   abort('404');
        $checkUnique = Addtion::where("name_ar","=",$request->name_ar)
            ->where("id","!=", $productAddtion->id)
            ->where("product_id","=",$product->id)
            ->get();
        if(!$checkUnique->isEmpty())
        {
            
            Session::flash('danger','الاسم بالعربى للاضافه موجود مسبقا ف هذا الصنف');
            return back();
        }
        $checkUnique = Addtion::where("name_en","=",$request->name_en)
            ->where("id","!=", $productAddtion->id)
            ->where("product_id","=",$product->id)
            ->get();
        if(!$checkUnique->isEmpty())
        {
            Session::flash('danger','الاسم لانجليزى للاضافه موجود مسبقا ف هذا الصنف');
            return back();
        }
        $productAddtion->product_id = $product->id;
        $productAddtion->note       = $request->edit_note;
        $productAddtion->price      = $request->edit_price;
        $productAddtion->quantity   = $request->edit_quantity;
        $productAddtion->name_ar    = $request->edit_name_ar;
        $productAddtion->name_en    = $request->edit_name_en;
        
        $productAddtion->save();
        Report(Auth::user()->id,'بتعديل اضافه لمنتج  '.$product->name_ar." ل ".$shop->name_ar);
        Session::flash('success','تم حفظ التعديلات');
        // dd($shop);
        return back();
    }
    // delete addtion from product
    public function deleteAddtion(Request $request)
    {
        // dd($request->all());
        $shop           = Shop::findOrFail($request->shop_id);
        
        $product        = $shop->Products()->where("id","=",$request->product_id)->first();
        $productAddtion = Addtion::findOrFail($request->id);   
        $productAddtion->delete();
        Report(Auth::user()->id,'بحذف  اضافه منتج  '.$product->name_ar." ل ".$shop->name_ar);
         Session::flash('success','تم الحذف ');
        
        return back();
    }
    // add size to product
    public function addsize(Request $request)
    {
        $niceError = array(
            'quantity.min'                   =>  '  الكميه لاتكون قيمه سالبه ',
            'quantity.required'              =>  '  كمية المنتج مطلوبه ',
            'price.min'                      =>  '  السعر لاتكون قيمه سالبه ',
            'price.required'                 =>  '  السعر المنتج مطلوبه ',
            'size_product_id.required'       =>  ' المنتج مطلوب ',
            'size_shop_id.required'          =>  ' المحل  مطلوب ',
            "size_product_id.exists"         => " المنتج  غير موجود ",
            "size_shop_id.exists"            => " المحل  غير موجود ",
            "edit_addtion_id.exists"         => " الاضافه  غير موجود ",
            "edit_addtion_id.required"       =>  ' الاضافه  مطلوب ',
            "size.required"                  =>  ' الحجم  مطلوب ',
            "size.unique"                    =>  ' الحجم  موجد سابقا ',
        );
         $this->validate($request,[
             'quantity'                =>"required|numeric|min:0",
             'price'                   =>"required|numeric|min:0",
             "size_product_id"         =>"required|exists:products,id",
             'size_shop_id'            =>"required|exists:shops,id",
             "size"                    =>"required|unique:sizes,size"
         ], $niceError);
         $shop              = Shop::findOrFail($request->size_shop_id);
         $product           = $shop->Products()->where("id","=",$request->size_product_id)->first();
         if(!$product)   abort('404');
         $size              = new Size;
         $size->size        = $request->size;
         $size->price       = $request->price;
         $size->quantity    = $request->quantity;
         $size->product_id  =  $product->id;
         $size->save();
         Report(Auth::user()->id,'بأضافة حجم جديد ل'.$product->name_ar);
         return back();

    }
    // edit product size
    public function editsize(Request $request)
    {
        // dd($request->all());
        $shop           = Shop::findOrFail($request->size_shop_id);
        $product        = $shop->Products()->where("id","=",$request->size_product_id)->first();
        if(!$product)   abort('404');
        $size           = $product->Sizes()->where("id","=",$request->id)->first();
        if(!$size)   abort('404');
        $niceError      = array(
            'quantity.min'                   =>  '  الكميه لاتكون قيمه سالبه ',
            'quantity.required'              =>  '  كمية المنتج مطلوبه ',
            'price.min'                      =>  '  السعر لاتكون قيمه سالبه ',
            'price.required'                 =>  '  السعر المنتج مطلوبه ',
            'size_product_id.required'       =>  ' المنتج مطلوب ',
            'size_shop_id.required'          =>  ' المحل  مطلوب ',
            "size_product_id.exists"         => " المنتج  غير موجود ",
            "size_shop_id.exists"            => " المحل  غير موجود ",
            "edit_addtion_id.exists"         => " الاضافه  غير موجود ",
            "edit_addtion_id.required"       =>  ' الاضافه  مطلوب ',
            "size.required"                  =>  ' الحجم  مطلوب ',
            "size.unique"                    =>  ' الحجم  موجد سابقا ',
        );
         $this->validate($request,[
             'quantity'                =>"required|numeric|min:0",
             'price'                   =>"required|numeric|min:0",
             "size_product_id"         =>"required|exists:products,id",
             'size_shop_id'            =>"required|exists:shops,id",
             "size"                    =>"required|unique:sizes,size,".$size["id"]
         ], $niceError);
         $size->size        = $request->size;
         $size->price       = $request->price;
         $size->quantity    = $request->quantity;
         $size->save();
         Report(Auth::user()->id,'بتعديل حجم لمنتج  '.$product->name_ar." ل ".$shop->name_ar);
         Session::flash('success','تم حفظ التعديلات');
        // dd($shop);
        return back();

    }
     // delete size from product
     public function deletesize(Request $request)
     {
         // dd($request->all());
         $shop           = Shop::findOrFail($request->shop_id);
         $product        = $shop->Products()->where("id","=",$request->product_id)->first();
         $size           = size::findOrFail($request->id);   
         $size->delete();
         Report(Auth::user()->id,'بحذف  حجم منتج  '.$product->name_ar." ل ".$shop->name_ar);
          Session::flash('success','تم الحذف ');
          // dd($shop);
         return back();
     }
   
}
