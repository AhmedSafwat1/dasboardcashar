<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    // user make
    public function User()
    {
        return $this->belongsTo('App\User');
    }
    // shop have
    public function Shop()
    {
        return $this->belongsTo('App\Shop');
    }

}
