<?php

namespace App;

use App\Shop_activity;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    //shops have Activty
    protected $fillable = ['name_ar', 'name_en'];
    public function ShopsActivity()
    {
        return $this->hasMany(Shop_activity::class);
    }
}
