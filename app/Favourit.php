<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favourit extends Model
{
    //
    #user favourit
    public function User()
    {
        return $this->belongsTo('App\user');
    }
    #product
    public function Product()
    {
        return $this->belongsTo('App\Product');
    }
}
