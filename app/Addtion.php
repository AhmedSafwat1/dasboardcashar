<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Addtion extends Model
{
    //
    protected $fillable = ['name_ar', 'name_en',"price","note","quantity","product_id"];
    //shops
   
    //Category
    public function Product()
    {
        return $this->belongsTo('App\Product');
    }
    //Addtions
    public function Addtions()
    {
        return $this->hasMany('App\Addtion');
    }
}
