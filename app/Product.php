<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //Category
    public function Category()
    {
        return $this->belongsTo('App\Category');
    }
    #shop
    public function Shop()
    {
        return $this->belongsTo('App\Shop');
    }
    //Sizes
    public function Sizes()
    {
        return $this->hasMany('App\Size');
    }
    #addtion for products
    public function Addtions()
    {
        return $this->hasMany('App\Addtion');
    }
    #favourit for products
    public function Favourits()
    {
        return $this->hasMany('App\Favourit');
    }
}
