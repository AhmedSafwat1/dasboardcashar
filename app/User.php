<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','phone','mobile','code','device_id',
        'lat','lng','last','city_id','birth'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function Role()
    {
        return $this->hasOne('App\Role','id','role');
    }
    #rates
    public function Rates()
    {
        return $this->hasMany('App\Rate');
    }
    // for city key
    public  function  City()
    {
        return $this->belongsTo('App\City');
    }
    #addtion
    public function ProductAddtions()
    {
        return $this->hasMany('App\Product_addtion');
    }
    #favourit for products
    public function Favourits()
    {
        return $this->hasMany('App\Favourit');
    }
    #card for users
    public function Cards()
    {
        return $this->hasMany('App\Card');
    }
    #orders for users
    public function Orders()
    {
        return $this->hasMany('App\Order');
    }
    #trnsefer money for users
    public function MoneyTransfers()
    {
        return $this->hasMany('App\MoneyTransfers');
    }

    public function Reports()
    {
        return $this->hasMany('App\Report','user_id','id');
    }
    

}
