<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->float("lat")->nullable();
            $table->float("lng")->nullable();
            $table->string("address")->nullable();
            $table->integer('status')->unsigned()->default("0");
            $table->float("total price")->unsigned()->default("0");
            $table->integer('user_id')->unsigned();
            $table->integer('shop_id')->unsigned();
            $table->float("delivery_coast")->default("0");
            $table->integer("prepared")->unsigned()->default(0);
            $table->string("Payment")->nullable();
            $table->foreign('shop_id')->references('id')->on('shops')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
